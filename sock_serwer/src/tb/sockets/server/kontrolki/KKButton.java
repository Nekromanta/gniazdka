/**
 * 
 */
package tb.sockets.server.kontrolki;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class KKButton extends JButton {

	private BufferedImage[] rysunki = new BufferedImage[3];
	private int stan = 0;	
	private int fieldNum;
	
	public KKButton(int num) {
		super("");
		fieldNum=num;
		setBackground(Color.WHITE);
			
 		for (int i=0;i<3; i++) {
 			BufferedImage tI = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
 			Graphics g = tI.getGraphics();
 			g.setColor(Color.GREEN);
 			switch (i) {
			case 0:
				break;
			case 1:
				g.drawOval(3, 3, 17, 17);
				break;
			case 2:
			default:
				g.drawLine(3,3,17,17);
				g.drawLine(3, 17, 17, 3);
				break;
			}
 			rysunki[i]=tI;
 		}
		
		/*
			BufferedImage tI = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
			tI.getGraphics().setColor(new Color(0, 0, 0, 255));
			rysunki[0] = tI;
			
			tI = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
			tI.getGraphics().setColor(new Color(0, 0, 0, 255));
			tI.getGraphics().drawOval(3, 3, 17, 17);
			rysunki[1] = tI;
			
			tI = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
			tI.getGraphics().setColor(new Color(0, 0, 0, 255));
			tI.getGraphics().drawRect(3,3,17,17);
			rysunki[2] = tI;
		*/
	}
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.drawImage(rysunki[stan], 0, 0, g.getClipBounds().width-10, g.getClipBounds().height-10 , null);
	}
	
	public int getState(){
		return stan;
		
	}
	
	public int getNumber(){
		return fieldNum;
	}
	
	public void setState(int s){
		stan=s;
	}
}
