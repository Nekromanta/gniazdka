package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Konsola {

	private static ServerSocket sSock;
	private static Socket sock;
	private static DataInputStream in;
	private static BufferedReader is;
	
	public static void main(String[] args) {
		Boolean loop=true;
		try {
			sSock = new ServerSocket(6666);
			sock = sSock.accept();
			in = new DataInputStream(sock.getInputStream());
			is = new BufferedReader(new InputStreamReader(in));
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(loop) {
			try {
				int input=in.readInt();
				System.out.println("Przeczytano z gniazdka: " + input);
				if(is.read()==5)
					loop=false;
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				loop=false;
			}
		}
		
		try {
			is.close();
			in.close();
			sock.close();
			sSock.close();
		} catch (IOException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
