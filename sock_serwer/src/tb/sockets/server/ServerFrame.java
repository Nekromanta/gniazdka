package tb.sockets.server;

import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import tb.sockets.server.OrderPane;
import tb.sockets.server.kontrolki.KKButton;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.border.LineBorder;

public class ServerFrame extends JFrame {

	private static JPanel contentPane;

	private int player;
	
	private static ServerSocket sSock;
	private static Socket sock;
	private static DataInputStream in;
	private static DataOutputStream so;
	private static JPanel panel_1;
	private static Boolean receiver = false;;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerFrame frame = new ServerFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		/*
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				int a = 0;
				int b = 0;
				while(receiver) {
						try {
							a=in.readInt();
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(a!=b) {
						((KKButton) panel_1.getComponent(a)).setState(2);
						b=a;	
					}
				}
			}
		});
		*/
	}
	

	/**
	 * Create the frame.
	 */
	public ServerFrame() {
		player=1;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		try {
			sSock = new ServerSocket(6666);
			sock = sSock.accept();
			in = new DataInputStream(sock.getInputStream());
			so = new DataOutputStream(sock.getOutputStream());
			receiver=true;
		} catch (IOException ee) {
					// TODO Auto-generated catch block
			ee.printStackTrace();			
		}
		
		OrderPane panel = new OrderPane();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 138, 123, 123);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(3, 3, 5, 5));
		for(int i=0;i<9;i++) {
			JButton btn1_1 = new KKButton(i);
			btn1_1.addActionListener(new ActionListener() {
			
				@Override
				public void actionPerformed(ActionEvent e) {
					if(((KKButton) btn1_1).getState()==0) {
						((KKButton) btn1_1).setState(player);
						try {
							int a=((KKButton) btn1_1).getNumber();
							so.writeInt(a);
							System.out.println(a+" ");
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			});
			panel_1.add(btn1_1);
		}
	}
}
